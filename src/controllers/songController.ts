import {Song} from '../models';
import Bookshelf from '../database/bookshelf';
import * as Joi from 'joi';
import dbConfig from "../database/dbConfig";
import {songCreateInputValidation} from "../utilities/validation/songValidation";


export const getAllSongs = async (req, res) => {
    //get all songs from database
    const songs = await new Song().orderBy(dbConfig.tables.song.params.name).fetchAll();

    if (!songs) {
        return res.status(404).json();
    }
    return res.status(200).json(songs);
};

export const getDetailOfSong = async (req, res) => {
    const songID = req.params.id;
    const song = await new Song().where({id: songID, is_approved: true}).fetch();

    if (!song) {
        return res.status(404).json();
    }
    return res.status(200).send(song);
};

export const addSong = async (req, res) => {

    //Validate song input
    const validatedBody = Joi.validate(req.body, songCreateInputValidation);

    if (validatedBody.error) {
        return res.status(400).json();
    }
    //validate if input song has unique name
    const inputName = req.body.name;
    const song = await new Song().where({name: inputName}).fetch();
    if (song) {
        return res.status(400).json();
    }

    //add song to database
    await Bookshelf.transaction(async (trx) => {
        const newSong = await new Song(req.body).save(null, {method: "insert", transacting: trx, returning: "*"});
        return newSong;
    });

    const songDat = await new Song().where({name: inputName}).fetch();
    if (!songDat) {
        return res.status(404).json();
    }

    return res.status(201).json(songDat);
};

export const updateSong = async (req, res) => {
    //get id number from url
    const songID = req.params.id;

    //get song with given ID
    const song = await new Song().where({id: songID}).fetch();
    if (!song) {
        return res.status(404).json();
    }

    //Validate song input
    const validatedBody = Joi.validate(req.body, songCreateInputValidation);
    if (validatedBody.error) {
        return res.status(400).json();
    }

    const isSameName = await new Song().where({name: req.body.name}).fetch();
    if (isSameName) {
        return res.status(404).json();
    }

    //update song to database
    const updatedSong = await Bookshelf.transaction(async (trx) => {
        const updatedSong = await song.save(req.body, {
            method: "update",
            transacting: trx,
            returning: "*",
            patch: true
        });
        return updatedSong;
    });
    return res.status(200).json(updatedSong);
};

export const deleteSong = async (req, res) => {

    //get id number from url
    const songID = req.params.id;

    //get song with given ID
    const song = await new Song().where({id : songID}).fetch();
    if (!song) {
        return res.status(404).json();
    }

    //delete song from database
    song.destroy();
    return res.status(204).json();

};
