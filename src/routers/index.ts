import authRouter from './authRouter';
import songRouter from './songRouter';

export {
    authRouter, songRouter
};
