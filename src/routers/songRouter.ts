import * as express from 'express';
import {getAllSongs, getDetailOfSong, addSong, updateSong, deleteSong} from "../controllers/songController";

const router = express.Router();

router.get('/songs', getAllSongs);
router.get('/songs/:id', getDetailOfSong);
router.post("/songs", addSong);
router.patch("/songs/:id", updateSong);
router.delete("/songs/:id", deleteSong);

export default router;
