import * as express from 'express';
import {auth} from "../controllers/authController";

const router = express.Router();

router.post('/auth', auth);

export default router;