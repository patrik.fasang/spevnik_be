import * as Knex from "knex";
import * as Promise from 'bluebird';
import dbConfig from '../dbConfig';

const tableConfig = dbConfig.tables.song;
const timestampJSON = dbConfig.tables.timestamps;

exports.up = function (knex: Knex): Promise<any> {
    return knex.schema.createTable(tableConfig.name, function (table) {
        table.increments().unique().primary().unsigned();
        table.string(tableConfig.params.name).notNullable();
        table.text(tableConfig.params.content).notNullable();
        table.boolean(tableConfig.params.is_approved).notNullable().defaultTo(true);
        table.integer(tableConfig.params.creator_id).notNullable();
        table.timestamp(timestampJSON.created).defaultTo(knex.fn.now());
        table.timestamp(timestampJSON.modified).defaultTo(knex.fn.now());
    });
};

exports.down = function (knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists(tableConfig.name);
};