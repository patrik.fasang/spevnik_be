import * as Knex from "knex";
import * as Promise from 'bluebird';
import dbConfig from '../dbConfig';

const UserJSON = dbConfig.tables.user;
const timestampJSON = dbConfig.tables.timestamps;

exports.up = function (knex: Knex): Promise<any> {
    return knex.schema.createTable(UserJSON.name, function (table) {
        table.increments().unique().primary().unsigned();
        table.string(UserJSON.params.firstName).notNullable();
        table.string(UserJSON.params.lastName).notNullable();
        table.string(UserJSON.params.email).notNullable().unique();
        table.string(UserJSON.params.password).notNullable();
        table.integer(UserJSON.params.userRole).unsigned().notNullable();
        table.boolean(UserJSON.params.isRemoved).notNullable().defaultTo(false);
        table.timestamp(timestampJSON.created).defaultTo(knex.fn.now());
        table.timestamp(timestampJSON.modified).defaultTo(knex.fn.now());
    });
};

exports.down = function (knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists(UserJSON.name);
};