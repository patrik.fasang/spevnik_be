import * as Knex from "knex";
import * as Promise from 'bluebird';
import dbConfig from "../../dbConfig";

exports.seed = function (knex: Knex): Promise<any> {
    return knex(dbConfig.tables.song.name).del()
        .then(function () {
            return knex(dbConfig.tables.song.name).insert([
                {
                    name: "Horicka zelena",
                    content: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium " +
                        "doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis " +
                        "et quasi architecto beatae vitae dicta sunt explicabo",
                    creator_id: "1",
                },
                {
                    name: "Naco som sa na tento svet narodil",
                    content: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium " +
                        "doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis " +
                        "et quasi architecto beatae vitae dicta sunt explicabo",
                    creator_id: "10",
                },
                {
                    name: "Z hrusovskiho kostolicka",
                    content: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium " +
                        "doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis " +
                        "et quasi architecto beatae vitae dicta sunt explicabo",
                    creator_id: "4",
                },
                {
                    name: "Naco pojdem domov",
                    content: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium " +
                        "doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis " +
                        "et quasi architecto beatae vitae dicta sunt explicabo",
                    creator_id: "1",
                },
                {
                    name: "Zkade ze to diovke boli",
                    content: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium " +
                        "doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis " +
                        "et quasi architecto beatae vitae dicta sunt explicabo",
                    creator_id: "7",
                },
                {
                    name: "Pod tym nasim vinohradom",
                    content: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium " +
                        "doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis " +
                        "et quasi architecto beatae vitae dicta sunt explicabo",
                    creator_id: "4",
                },
                {
                    name: "Na zelenej luke",
                    content: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium " +
                        "doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis " +
                        "et quasi architecto beatae vitae dicta sunt explicabo",
                    creator_id: "10",
                    is_approved: false,
                },
                {
                    name: "Nebudem sa zenit este",
                    content: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium " +
                        "doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis " +
                        "et quasi architecto beatae vitae dicta sunt explicabo",
                    creator_id: "10",
                    is_approved: false,
                },
                {
                    name: "Veje vetrik po ovsenom klase",
                    content: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium " +
                        "doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis " +
                        "et quasi architecto beatae vitae dicta sunt explicabo",
                    creator_id: "7",
                },
                {
                    name: "Chcem sa zenit, peniazky sporovat",
                    content: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium " +
                        "doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis " +
                        "et quasi architecto beatae vitae dicta sunt explicabo",
                    creator_id: "10",
                },
            ])
        })
}