let knexx = {
    development: {
        client: 'pg',
        connection: {
            host: '127.0.0.1',
            database: 'songbook_dev',
            user: 'postgres',
            password: 'admin',
            charset: 'utf8'
        },
        migrations:{
            directory: __dirname + '/migrations'
        },
        debug: false,
        seeds: {
            directory: __dirname + '/seeds/dev'
        }
    },
    production: {
        client: 'pg',
        connection: "postgres://lcxiuktxbctczl:d2d582f2071c109d35477eae85956bdb76939240e66b323d89fd4a4b39e7e9f3@ec2-46-137-113-157.eu-west-1.compute.amazonaws.com:5432/d2hikhdsnkooc7",
        migrations: {
            directory: __dirname + '/migrations'
        },
        debug: false,
        seeds: {
            directory: __dirname + '/seeds/dev'
        }
    }
};

export = knexx;
