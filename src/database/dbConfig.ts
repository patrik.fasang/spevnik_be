export default {
    dbOwner: 'postgres',

    tables: {
        user: {
            name: 'users',
            params: {
                id: 'id',
                firstName: 'firstName',
                lastName: 'lastName',
                email: 'email',
                password: 'password',
                userRole: 'userRole',
                isRemoved: 'isRemoved'
            }
        },
        song: {
            name: 'songs',
            params: {
                id: 'id',
                name: 'name',
                content: 'content',
                creator_id: 'creator_id',
                is_approved: 'is_approved',
            }
        },
        timestamps: {
            created: 'createdAt',
            modified: 'modifiedAt'
        }
    }
}
