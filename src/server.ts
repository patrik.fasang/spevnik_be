import * as router from "./routers";
import {verifyUser} from "./middlewares/jwtHelper";

const express = require('express');
const bodyParser = require("body-parser");

const app = express();

const port = process.env.PORT || 3000;
app
    .use(bodyParser.urlencoded({ extended: true }))
    .use(bodyParser.json())
    .use(router.authRouter)
    .use(router.songRouter)
    //.use(router.itemRouter);

app.listen(port);
console.log(`App is running on port: ${port}`);

export {app, port};
