export enum userRole {
    reader = 1,
    writer,
    admin
}