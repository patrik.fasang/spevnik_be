import * as Joi from 'joi'

export const songTestOutput = Joi.object().keys({
    id: Joi.number().integer().required(),
    name: Joi.string().required(),
    content: Joi.string().required(),
    is_approved: Joi.boolean().required(),
    creator_id: Joi.number().integer().required().min(1),
    createdAt: Joi.string().required(),
    modifiedAt: Joi.string().required()
});
export const songListTestOutput = Joi.array().items(songTestOutput);

export const songCreateInputValidation = Joi.object().keys({
    name: Joi.string().required(),
    content: Joi.string().required(),
    creator_id: Joi.number().integer().required().min(1),
});
