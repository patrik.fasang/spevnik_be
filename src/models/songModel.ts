import bookshelf from '../database/bookshelf';
import dbCfg from '../database/dbConfig';

export default class Song extends bookshelf.Model<Song> {

    hasTimestamps: string[] = [dbCfg.tables.timestamps.created, dbCfg.tables.timestamps.modified];

    get tableName() {
        return `${dbCfg.tables.song.name}`;
    }

}