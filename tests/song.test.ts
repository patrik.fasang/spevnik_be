import * as chai from "chai";
import * as Joi from "joi";
import {port} from "../src/server";
import knex from "../src/database/knex";
import {songListTestOutput, songTestOutput} from "../src/utilities/validation/songValidation";
import {login} from "../src/utilities/testHelper";

const app = "http://localhost:" + port;
const chaiHttp = require("chai-http");
chai.use(chaiHttp);
const expect = chai.expect, validEmail = 'john.doe@latasna.com', validPassword = '12345678';

let validToken: string;

describe("Song tests", () => {
    before(async () => {
        await knex.migrate.rollback();
        await knex.migrate.latest();
        await knex.seed.run();
        let res = await login(validEmail, validPassword);
        validToken = res.body.token;
    });

    const baseUrl = "/songs";
    const jsonType = "application/json";

    describe("GET all songs", () => {
        describe("Correct GET all songs", () => {
            it("returns 200", () => {
                return chai.request(app)
                    .get(`${baseUrl}`)
                    .set("token", validToken)
                    .then(res => {
                        expect(res.status).to.eq(200);
                        expect(res.type).to.eq(jsonType);
                        expect(res.body).to.be.an("array");
                        const validatedBody = Joi.validate(res.body, songListTestOutput);
                        expect(validatedBody.error).to.eq(null);
                    });
            });
        });
    });

    describe("GET detail of song", () => {
        describe("Correct GET", () => {
            it("returns 200", () => {
                return chai.request(app)
                    .get(`${baseUrl}/1`)
                    .set("token", validToken)
                    .then(res => {
                        expect(res.status).to.eq(200);
                        expect(res.type).to.eq(jsonType);
                        const validatedBody = Joi.validate(res.body, songTestOutput);
                        expect(validatedBody.error).to.eq(null);
                    });
            });
        });
        describe("Wrong ID", () => {
            it("returns 404", () => {
                return chai.request(app)
                    .get(`${baseUrl}/100`)
                    .set("token", validToken)
                    .catch(err => {
                        expect(err.response.type).to.eq("application/json");
                        expect(err.status).to.eq(404);
                    });
            });
        });
    });
    describe("POST create Song", () => {
        const inputBody = {
            name: "postTestSong",
            content: "Lorem Ipsum dolor sit amet",
            creator_id: 4
        };
        describe("Correct POST", () => {
            it("returns 201", () => {
                return chai.request(app)
                    .post(`${baseUrl}`)
                    .set("token", validToken)
                    .send(inputBody)
                    .then(res => {
                        expect(res.status).to.eq(201);
                        expect(res.type).to.eq(jsonType);
                        expect(res.body).to.be.an("object");
                        const validatedBody = Joi.validate(res.body, songTestOutput);
                        expect(validatedBody.error).to.eq(null);
                    });
            });
        });
        describe("Wrong POST input values", () => {
            const inputBody = {
                name: "postWrongSongValues",
                content: 2,
                creator_id: "badValue"
            };
            it("returns 400", () => {
                return chai.request(app)
                    .post(baseUrl)
                    .set("token", validToken)
                    .send(inputBody)
                    .then(res => {
                        expect(res.status).to.not.eq(201);
                    })
                    .catch(err => {
                        expect(err.status).to.eq(400);
                        expect(err.response.type).to.eq(jsonType);
                    });
            });
        });
        describe("Wrong POST Song with same name exists", () => {
            const inputBody = {
                name: "postSameNameSong",
                content: "Lorem Ipsum dolor sit amet",
                creator_id: 1
            };
            it("returns 400", () => {
                return chai.request(app)
                    .post(baseUrl)
                    .set("token", validToken)
                    .send(inputBody)
                    .then(res => {
                        return chai.request(app)
                            .post(baseUrl)
                            .set("token", validToken)
                            .send(inputBody)
                            .then(res => {
                                expect(res.status).to.not.eq(201);
                            })
                            .catch(err => {
                                expect(err.status).to.eq(400);
                                expect(err.response.type).to.eq(jsonType);
                            });
                    });
            });
        });
    });
    describe("PUT Update Song", () => {
        const inputBody = {
            name: "toUpdateSong",
            content: "Lorem Ipsum dolor sit amet",
            creator_id: 4
        };
        let songID: number;

        describe("PUT correct update", () => {
            it("returns 200", async () => {
                const createRes = await
                    chai.request(app)
                        .post(baseUrl)
                        .set("token", validToken)
                        .send(inputBody);
                songID = createRes.body.id;
                inputBody.name = "updatedSongCorrect";
                const res = await
                    chai.request(app)
                        .patch(`${baseUrl}/${songID}`)
                        .set("token", validToken)
                        .send(inputBody);
                expect(res.status).to.eq(200);
                expect(res.type).to.eq(jsonType);
                const validateBody = Joi.validate(res.body, songTestOutput);
                expect(validateBody.error).to.eq(null);
                return res;
            });
        });
        describe("PUT Wrong ID in URL", () => {
            it("returns 404", () => {
                const inputBody = {
                    name: "updateSongWrongID",
                    content: "Lorem Ipsum dolor sit amet",
                    creator_id: 4
                };
                return chai.request(app)
                    .patch(`${baseUrl}/999999`)
                    .set("token", validToken)
                    .send(inputBody)
                    .then(res => {
                        expect(res.status).to.not.eq(200);
                    })
                    .catch(err => {
                        expect(err.status).to.eq(404);
                        expect(err.response.type).to.eq(jsonType);
                    });
            });
        });
        describe("PUT Same song name", () => {
            const inputBody = {
                name: "toUpdateSongSameSong",
                content: "Lorem Ipsum dolor sit amet",
                creator_id: 4
            };
            it("returns 404", async () => {
                const createRes = await
                    chai.request(app)
                        .post(baseUrl)
                        .set("token", validToken)
                        .send(inputBody)
                const songID = createRes.body.id;
                inputBody.name = "updatedSongCorrect";
                const errRes = await
                    chai.request(app)
                        .patch(`${baseUrl}/${songID}`)
                        .set("token", validToken)
                        .send(inputBody)
                        .then(res => {
                            expect(res.status).to.not.eq(200);
                        })
                        .catch(err => {
                            expect(err.status).to.eq(404);
                            expect(err.response.type).to.eq(jsonType);
                        });
            });
        });
        describe("PUT Wrong fields", () => {
            const inputBody = {
                name: "toUpdateSongWrongFields",
                content: "Lorem Ipsum dolor sit amet",
                creator_id: 4
            };
            it("returns 400", async () => {
                const createRes = await
                    chai.request(app)
                        .post(baseUrl)
                        .set("token", validToken)
                        .send(inputBody)
                const songID = createRes.body.id;
                inputBody.content = "";
                const errRes = await
                    chai.request(app)
                        .patch(`${baseUrl}/${songID}`)
                        .set("token", validToken)
                        .send(inputBody)
                        .then(res => {
                            expect(res.status).to.not.eq(200);
                        })
                        .catch(err => {
                            expect(err.status).to.eq(400);
                            expect(err.response.type).to.eq(jsonType);
                        });
            });
        });
    });
    describe("DELETE Song", () => {
        const inputBody = {
            name: "toDeleteItem",
            content: "Lorem Ipsum dolor sit amet",
            creator_id: 1
        };
        let songID: number;

        describe("Correct DELETE", () => {
            it("returns 204",() =>{
                return chai.request(app)
                    .post(baseUrl)
                    .set("token", validToken)
                    .send(inputBody)
                    .then(res => {
                        songID = res.body.id;
                        return chai.request(app)
                            .del(`${baseUrl}/${songID}`)
                            .set("token", validToken)
                            .then(res => {
                                expect(res.status).to.eq(204);
                            });
                    });
            });
        });
        describe("DELETE Wrong ID in URL", () => {
            it("returns 404", () => {
                return chai.request(app)
                    .del(`${baseUrl}/999999`)
                    .set("token", validToken)
                    .send(inputBody)
                    .then(res => {
                        expect(res.status).to.not.eq(204);
                    })
                    .catch(err => {
                        expect(err.status).to.eq(404);
                        expect(err.response.type).to.eq(jsonType);
                    });
            });
        });

    });
});
